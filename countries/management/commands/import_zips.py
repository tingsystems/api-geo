__author__ = 'pollitux'
from django.core.management import BaseCommand
from countries.models import City, PostalCode
import csv


class Command(BaseCommand):
    help = ''

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        city = City.objects.get(id='57423aea5b0d6862e8684198')
        with open('cpsjqui.csv') as f:
            cps = csv.reader(f, delimiter=',')
            for cp in cps:
                new_cp = PostalCode(
                    city=city, zip=cp[0], neighborhood=cp[2], typeOfSettlement=cp[1]
                )
                new_cp.save()
                print('Zip {} import ok wih neighborhood {}'.format(new_cp.id, new_cp.neighborhood))
