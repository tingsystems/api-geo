import csv
from django.core.management import BaseCommand
from django.template.defaultfilters import slugify
from mongoengine import NotUniqueError, DoesNotExist
import time
from countries.models import State, City, Country


class Command(BaseCommand):
    help = ''

    def add_arguments(self, parser):
        parser.add_argument('op', nargs='+', type=str)

    def create_slug(self, value, instance):
        """
        Create slug of one instance
        :param value: name of the instance
        :param instance: Model to create slug
        :return: slug of the instance
        """
        try:
            params = {'slug': slugify(value)}
            instance.objects.get(**params)
            return slugify('{}{}'.format(value, int(time.time())))
        except DoesNotExist:
            return slugify(value)

    def handle(self, *args, **options):
        if options['op'][0] == 'importCities':
            print('Try import cities')
            country = Country.objects.get(id='574071705b0d680cb8f4101c')
            print(country)
            with open('cities.csv') as f:
                reader = csv.reader(f)
                for row in reader:
                    try:
                        state = State.objects.get(slug=slugify(row[1]))
                    except DoesNotExist:
                        state = State(
                            name=row[1], slug=slugify(row[1]), country=country
                        )
                        state.save()

                    city = City(
                        name=row[0],
                        slug=self.create_slug(row[0], City),
                        state=state
                    )
                    city.save()
                    print('Import city {} ok'.format(city.name))

        if options['op'][0] == 'deleteCities':
            states = State.objects.filter(country='574071705b0d680cb8f4101c')
            for state in states:
                cities = City.objects.filter(state=state)
                print(len(cities))



