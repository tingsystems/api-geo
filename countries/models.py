import datetime
from django_mongoengine import DynamicDocument
from django_mongoengine import fields


class Country(DynamicDocument):
    code = fields.StringField(max_length=20)
    name = fields.StringField(max_length=180)
    slug = fields.StringField(max_length=255, unique=True)
    createdAt = fields.DateTimeField(default=datetime.datetime.now, editable=False)

    def __str__(self):
        return self.name


class State(DynamicDocument):
    code = fields.StringField(max_length=20, null=True, blank=True)
    name = fields.StringField(max_length=180)
    slug = fields.StringField(max_length=255, unique=True)
    createdAt = fields.DateTimeField(default=datetime.datetime.now, editable=False)
    country = fields.ReferenceField(Country)

    def __str__(self):
        return self.name


class City(DynamicDocument):
    code = fields.StringField(max_length=20, null=True, blank=True)
    name = fields.StringField(max_length=180)
    slug = fields.StringField(max_length=255, unique=True)
    createdAt = fields.DateTimeField(default=datetime.datetime.now, editable=False)
    state = fields.ReferenceField(State)

    def __str__(self):
        return self.name


class PostalCode(DynamicDocument):
    zip = fields.StringField(max_length=10)
    neighborhood = fields.StringField(max_length=200)
    typeOfSettlement = fields.StringField(max_length=200)
    city = fields.ReferenceField(City)

    def __str__(self):
        return self.zip
