__author__ = 'pollitux'

from rest_framework_mongoengine import generics


class QueryParamsFilterMixin(generics.ListCreateAPIView):
    ordering = 'name'

    def parse_params(self, params):
        query_params = {}
        for key, value in params.items():
            if key == 'ordering':
                self.ordering = value
            if getattr(self.queryset.model, key, None):
                query_params.update({key: value})
        return query_params

    def get_queryset(self):
        return self.queryset.filter(**self.parse_params(self.request.query_params)).order_by(self.ordering)
