from rest_framework_mongoengine.serializers import DocumentSerializer
from .models import *


class CountrySerializer(DocumentSerializer):
    class Meta:
        model = Country
        fields = '__all__'


class StateSerializer(DocumentSerializer):
    class Meta:
        model = State
        fields = '__all__'


class CitySerializer(DocumentSerializer):
    class Meta:
        model = City
        fields = '__all__'

    def to_representation(self, instance):
        data = super(CitySerializer, self).to_representation(instance=instance)
        # get state info
        data['stateName'] = instance.state.name
        return data


class PostalCodeSerializer(DocumentSerializer):
    class Meta:
        model = PostalCode
        fields = '__all__'
