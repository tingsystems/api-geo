from rest_framework_mongoengine import generics
from .serializers import *
from .mixins import QueryParamsFilterMixin


class CountryListCreateView(generics.ListCreateAPIView):
    queryset = Country.objects
    serializer_class = CountrySerializer


class CountryRetrieveUpdateView(generics.RetrieveUpdateAPIView):
    queryset = Country.objects
    serializer_class = CountrySerializer
    lookup_field = 'pk'


class StateListCreateView(QueryParamsFilterMixin):
    queryset = State.objects
    serializer_class = StateSerializer


class StateRetrieveUpdateView(generics.RetrieveUpdateAPIView):
    queryset = State.objects
    serializer_class = StateSerializer
    lookup_field = 'pk'


class CityListCreateView(QueryParamsFilterMixin):
    queryset = City.objects
    serializer_class = CitySerializer


class CityRetrieveUpdateView(generics.RetrieveUpdateAPIView):
    queryset = City.objects
    serializer_class = CitySerializer
    lookup_field = 'pk'


class PostalCodeListCreateView(QueryParamsFilterMixin):
    queryset = PostalCode.objects
    serializer_class = PostalCodeSerializer
