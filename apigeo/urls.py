"""apigeo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django_mongoengine import mongo_admin
from countries.views import *

countries_url = [
    url(r'^countries$', CountryListCreateView.as_view(), name='countries'),
    url(r'^countries/(?P<pk>[-\w]+)$', CountryRetrieveUpdateView.as_view(), name='countries-retrieve'),
    url(r'^states$', StateListCreateView.as_view(), name='states'),
    url(r'^states/(?P<pk>[-\w]+)$', StateRetrieveUpdateView.as_view(), name='states-retrieve'),
    url(r'^cities$', CityListCreateView.as_view(), name='cities'),
    url(r'^cities/(?P<pk>[-\w]+)$', CityRetrieveUpdateView.as_view(), name='cities-retrieve'),
    url(r'^zips$', PostalCodeListCreateView.as_view(), name='zips'),
]

urlpatterns = [
    url(r'^api/v{}/'.format(1), include(countries_url)),
    url(r'^admin/', mongo_admin.site.urls),
]
